<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\{
    CartController, ProductController, OrderController, PaymentController
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

Route::controller(PaymentController::class)->group(function() {
    Route::get('payment/create', 'create')->name('payment.create');
    Route::post('payment/verify', 'verify')->name('payment.verify');
});

Route::get('products', [ProductController::class, 'index'])->name('products.index');

Route::controller(CartController::class)->group(function(){
    Route::get('checkout', 'checkout')->name('checkout');
    Route::get('cart/index','index')->name('cart.index');
    Route::post('cart/store','store')->name('cart.store');
    Route::post('cart/remove','remove')->name('cart.remove');
    Route::post('cart/sync','syncCount')->name('cart.sync');
});

Route::post('order/store', [OrderController::class, 'store'])->name('order.store');
