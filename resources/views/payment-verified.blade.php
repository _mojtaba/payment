<x-layout>
    <div class="text-gray-600">
        <h2>{{ __('Your order payed successfully') }}</h2>
        <ul>
            <li>tracking code: {{ $transaction->tracking_code }}</li>
            <li>You payed {{ number_format($transaction->payed) }} for:
                <ul>
                    @foreach($transaction->order->products as $product)
                        <li>{{ $product->name }} - count:"{{ $product->pivot->count }}"</li>
                    @endforeach
                </ul>
            </li>
        </ul>
    </div>
</x-layout>
