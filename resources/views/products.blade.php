<x-layout>
    <ul>
        @foreach($products as $product)
            <li>
                <form action="{{ route('cart.store') }}" method="post" class="text-gray-600 dark:text-gray-400 text-sm my-2">
                    @csrf
                    <input type="hidden" name="product" value="{{ $product->id }}">
                    <span class="font-bold text-lg pr-2">{{ $product->name }}</span>
                    <input type="number" min="1" value="1" name="count" class="dark:bg-black w-10">
                    <button class="px-3 py-2 ml-2 bg-green-600 rounded-md text-white">Add to card</button>
                </form>
            </li>
        @endforeach
    </ul>
</x-layout>
