@props(['order'])
<li>
    <form action="{{ route('cart.remove') }}" method="post" class="inline-block mr-2">
        @csrf
        <input type="hidden" value="{{ $order->product('id') }}" name="product">
        <button class="text-red-500">X</button>
    </form>
    <strong class="pr-5">{{ $order->product('name') }}</strong>
    <div class="pr-3 inline-block"><strong>Price:</strong> {{ $order->product('price') }}</div>
    <div class="pr-3  inline-block"><strong>Count:</strong> {{ $order->count() }}</div>
    <div class="pr-3  inline-block"><strong>Total: </strong>{{ $order->total() }}</div>
</li>
