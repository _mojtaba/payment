<x-layout>
    <ul class="text-gray-600 dark:text-gray-400">
        @foreach($order_items as $item)
            <x-order-item :order="$item" />
        @endforeach
    </ul>
</x-layout>
