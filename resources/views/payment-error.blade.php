<x-layout>
    <p class="text-red-500">
        {{ $exception->getMessage() }}
    </p>
</x-layout>
