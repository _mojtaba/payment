<x-layout>

    <ul class="text-gray-600 dark:text-gray-400">
        @foreach($order_items as $item)
            <x-order-item :order="$item" />
        @endforeach
        <li class="font-bold text-xl mt-2 ml-2">Payable: {{ number_format($total) }}</li>
    </ul>
    <div class="text-gray-600 dark:text-gray-400 border-l-2 border-gray-600">
        <form action="{{ route('order.store') }}" class="flex flex-col pl-3" method="post">
            @csrf
            <label for="card">
                Card Number
                <input type="text" name="card" class="w-full mb-2 p-2 rounded-md" id="card" value="{{ old('card', '') }}">
                @error('card') <span class="text-red-500">{{ $message }}</span> @enderror
            </label>
            <label for="name">
                Name
                <input type="text" name="name" class="w-full mb-2 p-2 rounded-md" id="name">
                @error('name') <span class="text-red-500">{{ $message }}</span> @enderror

            </label>
            <button class="bg-emerald-500 text-black py-2 rounded-md mt-5">Pay Now</button>
        </form>
    </div>

</x-layout>
