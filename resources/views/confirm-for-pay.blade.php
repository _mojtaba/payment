<x-layout>

    <ul class="text-gray-600 dark:text-gray-400">
        @foreach($items as $item)
            <x-order-item :order="$item" />
        @endforeach
        <li class="font-bold text-xl mt-2 ml-2">
            {{ __('You will pay:') }} <strong>{{ $payable }}</strong> {{ __('Rials') }}
        </li>
    </ul>
    <div class="text-gray-600 dark:text-gray-400 border-l-2 border-gray-600">
        {{ __('Check your information and if correct click on "Pay it" to pay') }}
        <form action="{{ route('order.store') }}" class="flex flex-col pl-3">
            @csrf
            <label for="card">
                {{ __('Card Number:') }} <strong>{{ $order->card }}</strong>
            </label>
            <label for="name">
                {{ __('Name:') }} <strong> {{ $order->name }}</strong>
            </label>
            <button class="bg-emerald-500 text-black py-2 rounded-md mt-5">{{ __('Pay it') }}</button>
        </form>
    </div>
</x-layout>

