<div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg text-gray-600 dark:text-gray-400 text-sm p-6">
    <a href="{{ route('cart.index') }}" style="margin-right: 15rem">ShopCart({{ count(session('cart') ?? []) }} items)</a>
    <a href="{{ route('checkout') }}">Checkout</a>
</div>
