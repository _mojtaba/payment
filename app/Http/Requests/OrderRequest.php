<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'card' => 'required|max:16|min:16',
            'name' => 'string'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'card' => str_replace(['-',' '], '', $this->card)
        ]);
    }
}
