<?php

namespace App\Http\Controllers;

use App\Exceptions\PaymentVerificationException;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Transaction;
use App\Paystar;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * @throws PaymentVerificationException
     */
    public function verify()
    {

        try {
            Paystar::checkPaymentStatus(request('status'));
            Paystar::checkPaymentCard(\request('card_number'));
            $transaction = Transaction::where('ref_id', request('ref_num'))->first();

            $transaction->update([
                'tracking_code' => request('tracking_code'),
                'gateway_transaction_id' => request('transaction_id'),
            ]);

            $payment = Paystar::verify($transaction);
            $transaction->update(['payed' => $payment->data->price]);
            $transaction->order()->update(['payed_at' => now()]);

            return view('payment-verified', compact('transaction'));
        } catch (PaymentVerificationException $exception)
        {
            return view('payment-error', compact('exception'));
        } catch (\Exception $e)
        {
            abort($e->getCode());
        }
    }
}
