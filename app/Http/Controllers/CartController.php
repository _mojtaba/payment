<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use App\Models\Cart;
use App\Models\Product;
use App\OrderItem;
use Illuminate\Support\Collection;


class CartController extends Controller
{

    public function index()
    {
        $order_items = Cart::orderItems();
        return view('cart', compact('order_items'));
    }

    public function checkout()
    {
        $order_items = Cart::orderItems();
        $total = (new Collection($order_items))->sum(fn($item) => $item->total());

        return view('checkout', compact('order_items', 'total'));
    }

    public function store(CartRequest $request): \Illuminate\Http\RedirectResponse
    {
        $cart = session('cart');
        if (isset($cart[$request->product])) {
            $cart[$request->product] += $request->count;
        } else {
            $cart[$request->product] = $request->count;
        }
        session()->put('cart', $cart);

        return redirect()->back()->with('success', __('Product successfully added to your cart'));
    }

    public function syncCount(CartRequest $request): \Illuminate\Http\RedirectResponse
    {
        $cart = session('cart');
        $cart[$request->product] = $request->count;
        session()->put('cart', $cart);
        return redirect()->back()->with('success', __('Product count successfully synced in your cart.'));

    }

    public function remove(): \Illuminate\Http\RedirectResponse
    {
        $cart = session('cart');
        unset($cart[request('product')]);
        session()->put('cart', $cart);
        return redirect()->back()->with('success', __('Product successfully removed from cart.'));
    }

}
