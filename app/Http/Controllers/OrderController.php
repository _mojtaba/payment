<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Paystar;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class OrderController extends Controller
{
    public function store(OrderRequest $request)
    {
        $total = Cart::totalPrice();

        $order = Order::firstOrCreate($request->validated() + ['payed_at' => null], compact('total'));
        $order->products()->sync(Cart::attachableFormat());

        return (new Paystar($order->total, $order->id, route('payment.verify')))
            ->setCardNumber($order->card)
            ->ready()->pay();
    }
}
