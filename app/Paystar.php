<?php

namespace App;

use App\Exceptions\PaymentPurchaseException;
use App\Exceptions\PaymentVerificationException;
use App\Models\Order;
use App\Models\Transaction;
use http\Client;
use Illuminate\Support\Facades\Http;

class Paystar extends Payment
{
    protected const SERVICE_URL = 'https://core.paystar.ir/api/pardakht';
    protected const CREATE_URL = self::SERVICE_URL . '/create';
    protected const PAYMENT_URL = self::SERVICE_URL . '/payment';
    protected const VERIFY_URL = self::SERVICE_URL . '/verify';

    protected $card_number;

    /*
     * purchase data ['token','ref_num', 'order_id', 'payment_amount']
     */
    protected $data;

    /**
     * generate hashed string via sha512 algorithm
     *
     * @param $data
     * @return string
     */
    protected static function getSign($data)
    {
        return hash_hmac('sha512', $data, self::getSignKey());
    }

    /**
     * checks two given sign are same and equal.
     *
     * @param $expected
     * @param $data
     * @return bool
     */
    public static function checkSign($expected, $data)
    {
        $hashed_data = hash_hmac('sha512', $data, self::getSignKey());

        if (!isset($expected) || !isset($data)) return false;
        if (strlen($expected) !== strlen($hashed_data)) return false;

        return md5($expected) === md5($hashed_data);
    }

    /**
     * return sign key from payment config file
     *
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    protected static function getSignKey()
    {
        return config('payment.paystar.sign_key');
    }

    /**
     * retrieves data object.
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * retrieves token from data object.
     *
     * @return mixed
     */
    public function getToken()
    {
        return $this->getData()->token;
    }

    /**
     * retrieves ref_num from data object.
     *
     * @return mixed
     */
    public function getRefId()
    {
        return $this->getData()->ref_num;
    }

    /**
     * retrieves payment amount from data object.
     *
     * @return mixed
     */
    public function getAmount()
    {
        return $this->getData()->payment_amount;
    }

    /**
     * set card number. it can be used to force user pay with given card number.
     *
     * @param $card_number
     * @return $this
     */
    public function setCardNumber($card_number)
    {
        $this->card_number = $card_number;
        return $this;
    }

    /**
     * create a request for get access to gateway for payment
     *
     * @throws PaymentPurchaseException
     */
    public function ready()
    {
        $response = $this->sendPurchaseRequest();
        $this->checkForPurchaseExceptions($response);
        $this->data = json_decode($response->body())->data;

        return $this;
    }

    /**
     * redirect user for pay transaction.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function pay()
    {
        $this->createTransaction();
        return redirect(self::PAYMENT_URL . '?token=' . $this->getToken());
    }

    /**
     * verifies given payment request.
     * @throws PaymentVerificationException
     */
    public static function verify(Transaction $transaction)
    {
        $response = self::sendVerifyRequest($transaction);
        self::checkVerifyException($response);

        return json_decode($response->body());
    }

    /**
     * send request to check payment is valid.
     *
     * @param Transaction $transaction
     * @return \GuzzleHttp\Promise\PromiseInterface|\Illuminate\Http\Client\Response
     */
    private static function sendVerifyRequest(Transaction $transaction)
    {
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . config('payment.paystar.gateway_id')
        ];

        $sign_data = implode('#', [
            $transaction->payable, $transaction->ref_id,
            $transaction->card_number, $transaction->tracking_code
        ]);

        $attributes = [
            'ref_num' => $transaction->ref_id,
            'amount' => $transaction->payable,
            'sign' => self::getSign($sign_data)
        ];

        return Http::withHeaders($headers)->post(self::VERIFY_URL, $attributes);
    }

    /**
     * check if verification request response has error throws an exception
     *
     * @param $response
     * @return void
     * @throws PaymentVerificationException
     */
    private static function checkVerifyException($response)
    {
        if (!$response->ok())
            throw new PaymentVerificationException(json_encode($response->body())->message);
    }


    /**
     * @throws PaymentVerificationException
     */
    public static function checkPaymentStatus($status)
    {
        if ($status == 1) return true;

        $status = self::codeDescription($status);

        if (isset($status))
            throw new PaymentVerificationException(key($status) . ' - ' . end($status));

        throw new PaymentVerificationException($status);
    }

    /**
     * @throws PaymentVerificationException
     */
    public static function checkPaymentCard($card_number)
    {
        $order_card = substr_replace(Order::find(request()->order_id)->card, '******', '6', '6');
        if ($card_number != $order_card)
            throw new PaymentVerificationException("wrong card number");
    }


    /*
     * check response has error or response is successful
     */
    private function checkForPurchaseExceptions($response)
    {
        if (!$response->ok())
            throw new PaymentPurchaseException(json_decode($response->body())->message);
    }

    /*
     * send post request for create payment connection
     */
    private function sendPurchaseRequest()
    {
        $attributes = [
            'amount' => $this->amount,
            'order_id' => $this->order,
            'callback' => $this->callback,
            'sign' => $this->getSign($this->amount . '#' . $this->order . '#' . $this->callback),
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . config('payment.paystar.gateway_id')
        ];

        if (isset($this->card_number)) {
            $attributes += ['card_number' => $this->card_number];
        }

        return Http::withHeaders($headers)->post(self::CREATE_URL, $attributes);
    }

    private function createTransaction()
    {
        $order = Order::find($this->order);

        Transaction::create([
            'token' => $this->getToken(),
            'ref_id' => $this->getRefId(),
            'order_id' => $order->id,
            'card_number' => $order->card,
            'total' => $order->total,
            'payable' => $this->getAmount()
        ]);
    }

    public static function codeDescription($code)
    {
        $table = [
            '1' => ['Ok' => ' موفق'],
            '-1' => ['invalidRequest' => 'درخواست نامعتبر (خطا در پارامترهای ورودی)'],
            '-2' => ['inactiveGateway' => 'درگاه فعال نیست'],
            '-3' => ['retryToken' => 'توکن تکراری است'],
            '-4' => ['amountLimitExceed' => 'مبلغ بیشتر از سقف مجاز درگاه است'],
            '-5' => ['invalidRefNum' => 'شناسه ref_num معتبر نیست'],
            '-6' => ['retryVerification' => 'تراکنش قبلا وریفای شده است'],
            '-7' => ['badData' => ' پارامترهای ارسال شده نامعتبر است'],
            '-8' => ['trNotVerifiable' => 'تراکنش را نمیتوان وریفای کرد'],
            '-9' => ['trNotVerified' => 'تراکنش وریفای نشد'],
            '-98' => ['paymentFailed' => 'تراکنش ناموفق'],
            '-99' => ['error' => 'خطای سامانه'],
        ];

        if (isset($table[$code])) return $table[$code];
        return null;
    }
}
