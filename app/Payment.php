<?php

namespace App;

abstract class Payment
{
    protected $amount;
    protected $order;
    protected $callback;

    public function __construct($amount, $order, $callback)
    {
        $this->amount = $amount;
        $this->order = $order;
        $this->callback = $callback;
    }

    public abstract function ready();

    public abstract function pay();

}
