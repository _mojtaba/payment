<?php

namespace App;

use App\Models\Product;

class OrderItem implements \JsonSerializable
{
    protected array $product;
    protected int $count;

    public function __construct(Product $product, int $count)
    {
        $this->product = ['name' => $product->name, 'id' => $product->id, 'price' => $product->price];
        $this->count = $count;
    }

    /**
     * returns total price.
     *
     * @return float|int
     */
    public function total()
    {
        return $this->product['price'] * $this->count;
    }

    /**
     * returns count of this item.
     * @return int
     */
    public function count()
    {
        return $this->count;
    }

    /**
     * return product object or given field from product.
     *
     * @param string $field
     * @return array|mixed
     */
    public function product($field = null)
    {
        if (!isset($field)) return $this->product;
        return $this->product[$field];
    }

    public function jsonSerialize(): mixed
    {
        return ['product' => $this->product, 'count' => $this->count, 'total' => $this->total()];
    }
}
