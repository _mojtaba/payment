<?php

namespace App\Models;

use App\OrderItem;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Cart extends Model
{
    public static function orderItems()
    {
        $orders = [];
        Product::whereIn('id', array_keys(session('cart') ?? []))
            ->each(function ($product) use (&$orders) {
                $orders[] = new OrderItem($product, session('cart')[$product->id]);
            });
        return $orders;
    }

    public static function totalPrice()
    {
        $order_items = new Collection(self::orderItems());
        return $order_items->sum(fn($item) => $item->total());
    }

    public static function attachableFormat()
    {
        $order_items = new Collection(self::orderItems());
        $order_items->each(function ($item) use (&$items) {
            $items[$item->product('id')] = ['count' => $item->count()];
        });
        return $items;
    }
}
