<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected $card_number = '5555-1111-2222-3333';

    public function mockHttpRequest($order = 1, $payment = 5000, $token = 'lorem')
    {
        Http::fake(
            [
                "https://core.paystar.ir/api/pardakht/create*"
                => Http::response([
                    'status' => 1, 'message' => 'Ok',
                    "data" => [
                        "token" => $token,
                        "ref_num" => 'random_num',
                        "order_id" => $order,
                        "payment_amount" => $payment
                    ]
                ]),
            ]

        );
    }

    public function paymentServiceWillRedirectToCallback($status = 'Ok')
    {
        Redirect::shouldReceive('to')->andReturn(response([
            'status' => $status, 'order_id' => 1, 'ref_num' => 'random_num',
            'transaction_id' => rand(10000, 100000),
            'card_number' => substr_replace(
                str_replace('-','',$this->card_number),
                '******', '6', '6'),
            'tracking_code' => str_shuffle(md5(time()))
        ]));
    }
}
