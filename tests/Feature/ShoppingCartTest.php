<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShoppingCartTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_add_product_to_shop_cart()
    {
        $product = Product::factory(2)->create();

        $this->post(route('cart.store'), ['product' => $product[0]->id, 'count' => 1]);
        $this->post(route('cart.store'), ['product' => $product[0]->id, 'count' => 1]);
        $this->post(route('cart.store'), ['product' => $product[1]->id, 'count' => 5]);
        $this->assertEquals(session('cart'), [$product[0]->id => 2, $product[1]->id => 5]);
    }

    public function test_user_can_remove_product_from_cart()
    {
        $product = Product::factory(2)->create();

        $this->post(route('cart.store'), ['product' => $product[0]->id, 'count' => 3]);
        $this->post(route('cart.store'), ['product' => $product[1]->id, 'count' => 2]);
        $this->post(route('cart.remove'), ['product' => $product[1]->id]);

        $this->assertEquals(session('cart'), [$product[0]->id => 3]);

    }

    public function test_user_can_sync_order_count()
    {
        $product = Product::factory()->create();

        $this->post(route('cart.store'), ['product' => $product->id, 'count' => 2]);
        $this->post(route('cart.sync'), ['product' => $product->id, 'count' => 5]);

        $this->assertEquals(session('cart'), [$product->id => 5]);

        $this->post(route('cart.sync'), ['product' => $product->id, 'count' => 1]);
        $this->assertEquals(session('cart'), [$product->id => 1]);

    }

    public function test_user_can_see_cart_details()
    {
        $this->withoutExceptionHandling();
        $product = Product::factory(2)->create();

        $this->post(route('cart.store'), ['product' => $product[0]->id, 'count' => 3]);
        $this->post(route('cart.store'), ['product' => $product[1]->id, 'count' => 2]);

        $this->get(route('cart.index'))->assertSee(
            [  $product[0]->name, $product[0]->price,
                $product[1]->name, $product[1]->price]
        );
    }
    public function test_product_must_exists_in_database()
    {
        $product = Product::factory()->create();

        $this->post(route('cart.store'), ['product' => 1999, 'count' => 1])
            ->assertSessionHasErrors();
    }

    public function test_product_count_must_at_least_one()
    {
        $product = Product::factory()->create();

        $this->post(route('cart.store'), ['product' => $product->id, 'count' => 0])
            ->assertSessionHasErrors();
    }
}
