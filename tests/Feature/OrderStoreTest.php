<?php

namespace Tests\Feature;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderStoreTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_store_cart_as_order()
    {
        $this->withoutExceptionHandling();
        $product = Product::factory(3)->create();

        $this->post(route('cart.store'), ['product' => $product[0]->id, 'count' => 1]);
        $this->post(route('cart.store'), ['product' => $product[0]->id, 'count' => 1]);
        $this->post(route('cart.store'), ['product' => $product[1]->id, 'count' => 5]);

        $this->mockHttpRequest(1, Cart::totalPrice());
        $this->paymentServiceWillRedirectToCallback();

        $response = $this->post(route('order.store'), ['card' => '5555111222332243', 'name' => 'mojtaba']);

        $this->assertDatabaseHas('orders', ['card' => '5555111222332243', 'name' => 'mojtaba']);

        $this->assertTrue(Order::first()->products->contains($product[0]));
        $this->assertTrue(Order::first()->products->contains($product[1]));
        $this->assertFalse(Order::first()->products->contains($product[2]));
    }
}
