<?php

namespace Tests\Feature;

use App\Exceptions\PaymentVerificationException;
use App\Models\Order;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Tests\TestCase;

class OrderPayTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $product = Product::factory()->create(['price' => 100000]);
        $this->post(route('cart.store'), ['product' => $product->id, 'count' => 5]);
        $this->mockHttpRequest(1, 5 * 100000);
    }

    public function test_amount_and_ref_num_persistence()
    {
        $this->withoutExceptionHandling();
        $this->paymentOperation();
        $this->assertEquals(Transaction::first()->total, 5 * 100000);
        $this->assertDatabaseHas('transactions', ['ref_id' => 'random_num']);
    }

    public function test_invalid_payment_throws_exception()
    {
        $this->withoutExceptionHandling();
        $response = $this->followRedirects($this->paymentOperation('-1'))->content();
        $this->post(route('payment.verify'), json_decode($response, true))
            ->assertSee('invalidRequest');

    }

    public function test_payment_verifies_when_status_is_ok()
    {
        $this->withoutExceptionHandling();
        $response = $this->followRedirects($this->paymentOperation())->content();

        $transaction = Transaction::first();
        $must_return = [
            'status' => '1', 'message' => '',
            'data' => [
                'price' => $transaction->payable,
                'ref_num' => $transaction->ref_id,
                'card_number' => $transaction->card_number
            ]
        ];
        Http::fake(['*' => Http::response($must_return)]);
        $this->post(route('payment.verify'), json_decode($response, true));
        $transaction->refresh();
        $this->assertEquals($transaction->payed, $transaction->payable);
    }

    public function test_payment_throws_exception_when_card_number_is_different()
    {
        $this->withoutExceptionHandling();

        $response = $this->followRedirects(
            $this->paymentOperation('1', fn() => $this->card_number = '3336-5554-8879-8999')
        )->content();

        $transaction = Transaction::first();

        $must_return = [
            'status' => '1', 'message' => '',
            'data' => [
                'price' => $transaction->payable,
                'ref_num' => $transaction->ref_id,
                'card_number' => $transaction->card_number
            ]
        ];

        Http::fake(['*' => Http::response($must_return)]);

        $this->post(route('payment.verify'), json_decode($response, true))->assertSee('wrong card');

    }

    public function paymentOperation($status = '1', $before_payment_operation = null)
    {
        $this->paymentServiceWillRedirectToCallback($status);

        if(isset($before_payment_operation) && is_callable($before_payment_operation)) $before_payment_operation();

        return $this->startPayOperation();
    }


    public function startPayOperation()
    {
        return $this->post(
            route('order.store'),
            ['card' => $this->card_number, 'name' => 'mojtaba']
        );
    }


}
