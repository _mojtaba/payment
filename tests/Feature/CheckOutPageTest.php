<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CheckOutPageTest extends TestCase
{
    use RefreshDatabase;

    public function test_checkout_page_rendered_successfully(){
        $product = Product::factory()->create();
        $this->post(route('cart.store'), ['product' => $product->id, 'count' => 10]);

        $this->get(route('checkout'))
            ->assertStatus(200)
            ->assertSee($product->name);
    }

}
