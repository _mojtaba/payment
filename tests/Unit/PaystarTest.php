<?php

namespace Tests\Unit;

use App\Exceptions\PaymentPurchaseException;
use App\Paystar;
use Illuminate\Support\Facades\Http;
use Mockery\Mock;
use Tests\TestCase;

class PaystarTest extends TestCase
{
    public function test_user_can_create_payment()
    {
        $this->mockHttpRequest();

        $payment = new Paystar(5000, 1, '/');

        $this->assertObjectHasAttribute('token', $payment->ready()->getData());

    }

    public function test_service_throw_exception_on_server_error()
    {
        Http::fake(
            [
                'core.paystar.ir/*' => Http::response([
                    'status' => "unauthenticated", 'message' => 'درگاه شما شناسایی نشد!',
                    "action" => "PardakhtCreate",
                    "tag" => "unauthenticated",

                    "data" => []
                ], 401)]
        );

        $payment = new Paystar(5000, 1, '/');

        $this->expectExceptionObject(new PaymentPurchaseException("درگاه شما شناسایی نشد!"));
        $payment->ready();
    }

    public function test_it_can_check_signs()
    {
        config()->set('payment.paystar.sign_key', 'somekey');
        $sign = hash_hmac('sha512', '1#2#3', 'somekey');
        $this->assertTrue(Paystar::checkSign($sign, "1#2#3"));
        $this->assertFalse(Paystar::checkSign($sign, "1#2#3#4"));
    }

}
