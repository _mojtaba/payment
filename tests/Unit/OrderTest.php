<?php

namespace Tests\Unit;

use App\Models\Product;
use App\OrderItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderTest extends TestCase
{

    use RefreshDatabase;

    public function test_order_total_calc_by_price()
    {
        $product = Product::factory()->create();
        $count = 20;

        $order = new OrderItem($product, $count);

        $this->assertEquals($order->total(), $product->price * $count);

    }
}
